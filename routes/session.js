const express = require('express');
const { session } = require('../controllers');

const router = express.Router();

router.post('/update/sessionId', session.updateMovieDBSessionId);

module.exports = router;
