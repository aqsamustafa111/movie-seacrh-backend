const auth = require('./auth');
const session = require('./session');

module.exports = [auth, session];
