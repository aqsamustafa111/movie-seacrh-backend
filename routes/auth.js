const express = require('express');
const { auth } = require('../controllers');

const router = express.Router();

router.get('/signup', (req, res, next) => {
  console.log('signup');
  res.send('sign up backend');
});

router.post('/signup', auth.signup);

router.post('/signin', auth.signin);

module.exports = router;
