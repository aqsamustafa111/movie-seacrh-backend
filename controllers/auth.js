const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { secretKey } = require('../config');
const User = require('../models/user');

const {
  OK,
  NOT_FOUND,
  UNAUTHENTICATED,
  EXPECTATION_FAILED,
  CONFLICT,
} = require('../config/statusCodes');

exports.signup = async (req, res, next) => {
  console.log('signup', req.body);

  const { fullName, email, password: plainTextPassword } = req.body;

  const password = await bcrypt.hash(plainTextPassword, 10);

  const user = new User({
    fullName,
    email,
    password,
  });

  user
    .save()
    .then((result) => {
      console.log('result', result);
      res.status(OK).json({ status: 'success', user });
    })
    .catch((err) => {
      if (err.code === 11000) {
        res
          .status(CONFLICT)
          .json({ status: 'error', error: 'Email already in use' });
      } else {
        res
          .status(EXPECTATION_FAILED)
          .json({ status: 'error', error: err.message });
      }
    });
};

exports.signin = async (req, res, next) => {
  console.log('signin backend', req.body);
  const { email, password } = req.body;

  User.findOne({ email })
    .then(async (user) => {
      if (user) {
        if (await bcrypt.compare(password, user.password)) {
          const token = jwt.sign(
            {
              id: user._id,
              email: user.email,
            },
            secretKey
          );

          res.status(OK).json({ status: 'success', user, token });
        } else {
          res
            .status(UNAUTHENTICATED)
            .json({ status: 'error', error: 'Invalid password' });
        }
      } else {
        res
          .status(NOT_FOUND)
          .json({ status: 'error', error: 'Invalid email or password' });
      }
    })
    .catch((err) => {
      console.log(err);

      res
        .status(EXPECTATION_FAILED)
        .json({ status: 'error', error: err.message });
    });
};
