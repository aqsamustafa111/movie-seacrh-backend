const User = require('../models/user');

const { OK, EXPECTATION_FAILED } = require('../config/statusCodes');

exports.updateMovieDBSessionId = (req, res, next) => {
  console.log('update session id backend', req.body);

  User.findOneAndUpdate(
    { _id: req.body.user._id },
    { sessionId: req.body.sessionId },
    { new: true },
    (err, doc) => {
      if (err) {
        res
          .status(EXPECTATION_FAILED)
          .json({ status: 'error', error: err.message });
      }

      res.status(OK).json({ status: 'success', res: doc });
    }
  );
};
