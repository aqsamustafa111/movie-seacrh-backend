require('dotenv').config();

module.exports = {
  port: process.env.PORT,
  mongo_atlas_user: process.env.MONGO_USER,
  mongo_atlas_pw: process.env.MONGO_PASSWORD,
  secretKey: process.env.SECRET_KEY,
};
