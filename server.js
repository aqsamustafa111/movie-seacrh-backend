const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const routes = require('./routes');
const cors = require('cors');
const { port, mongo_atlas_pw } = require('./config');

mongoose
  .connect(
    `mongodb+srv://admin:${mongo_atlas_pw}@movie-search-users.ds7c9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then((res) => {
    console.log('Connection established with the database');
  });

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'static')));

app.use('/api', routes);

app.listen(port, () => {
  console.log(`server is listening on port ${port}`);
});
