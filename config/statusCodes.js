exports.OK = 200;
exports.NOT_FOUND = 404;
exports.CONFLICT = 409;
exports.UNAUTHENTICATED = 401;
exports.EXPECTATION_FAILED = 417;
